# Caravel User Project

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) [![UPRJ_CI](https://github.com/efabless/caravel_project_example/actions/workflows/user_project_ci.yml/badge.svg)](https://github.com/efabless/caravel_project_example/actions/workflows/user_project_ci.yml) [![Caravel Build](https://github.com/efabless/caravel_project_example/actions/workflows/caravel_build.yml/badge.svg)](https://github.com/efabless/caravel_project_example/actions/workflows/caravel_build.yml)

We have designed novel chaotic map circuits using a general framework for developing a multi-parameter 1-D chaotic system for uniform robust chaotic
operation, an important feature for diverse practical applications where parameter disturbance may cause degradation or even
complete disappearance of chaotic properties. The proposed system consists of a normalized linear combination of multiple 1-D seed
maps and can be developed using any number of seed maps with any coefficient used for linear combination. We have chosen five
representative combinations of three common seed maps, namely, logistic, tent, and sine map to demonstrate the system’s performance.
The wide uninterrupted chaotic range and improved chaotic properties were demonstrated with the aid of stability analysis, bifurcation
diagram, Lyapunov exponent, Kolmogorov entropy, Shannon entropy, and correlation coefficient. We also demonstrate how cascading
can be used for further performance improvement and compare our system with prior works.  The excellent performance along with the simplicity of construction makes the proposed system promising for diverse applications such as chaos-based cryptography, secure communication, reconfigurable logic gate, random number generation, and so on. Details can be found in the following paper:

M. S. Hasan, P. S. Paul, M. Sadia, and M. R. Hossain, “Design of a Weighted Average Chaotic System for Robust Chaotic Operation”, IEEE International Midwest Symposium on Circuits and Systems (MWSCAS), 2021.

## Please fill in your project documentation in this README.md file 

Refer to [README](docs/source/quickstart.rst) for a quick start of how to use caravel_user_project

Refer to [README](docs/source/index.rst) for this sample project documentation. 
